﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Main : Form
    {
        private DataTable dtCelular;
        public Main()
        {
            InitializeComponent();
        }

        private void datosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cell_Info ci = new Cell_Info();
            ci.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void buscarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GstCelular gc = new GstCelular();
            gc.Show();
        }
    }
}
