﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Atributos
{
    class Celular
    {
        private string tech;
        private string announ;
        private string stat;
        private string dimensions;
        private string weight;
        private string build;
        private string SIM;
        private string Type;
        private string size;
        private string resol;
        private string protect;
        private string OS;
        private string chipset;
        private string Cpu;
        private string GPU;
        private string CS;
        private string inter;

        public string Tech
        {
            get
            {
                return tech;
            }

            set
            {
                tech = value;
            }
        }

        public string Announ
        {
            get
            {
                return announ;
            }

            set
            {
                announ = value;
            }
        }

        public string Stat
        {
            get
            {
                return stat;
            }

            set
            {
                stat = value;
            }
        }

        public string Dimensions
        {
            get
            {
                return dimensions;
            }

            set
            {
                dimensions = value;
            }
        }

        public string Weight
        {
            get
            {
                return weight;
            }

            set
            {
                weight = value;
            }
        }

        public string Build
        {
            get
            {
                return build;
            }

            set
            {
                build = value;
            }
        }

        public string SIM1
        {
            get
            {
                return SIM;
            }

            set
            {
                SIM = value;
            }
        }

        public string Type1
        {
            get
            {
                return Type;
            }

            set
            {
                Type = value;
            }
        }

        public string Size
        {
            get
            {
                return size;
            }

            set
            {
                size = value;
            }
        }

        public string Resol
        {
            get
            {
                return resol;
            }

            set
            {
                resol = value;
            }
        }

        public string Protect
        {
            get
            {
                return protect;
            }

            set
            {
                protect = value;
            }
        }

        public string OS1
        {
            get
            {
                return OS;
            }

            set
            {
                OS = value;
            }
        }

        public string Chipset
        {
            get
            {
                return chipset;
            }

            set
            {
                chipset = value;
            }
        }

        public string Cpu1
        {
            get
            {
                return Cpu;
            }

            set
            {
                Cpu = value;
            }
        }

        public string GPU1
        {
            get
            {
                return GPU;
            }

            set
            {
                GPU = value;
            }
        }

        public string CS1
        {
            get
            {
                return CS;
            }

            set
            {
                CS = value;
            }
        }

        public string Inter
        {
            get
            {
                return inter;
            }

            set
            {
                inter = value;
            }
        }

        public Celular(string tech, string announ, string stat, string dimensions, string weight, string build, string sIM, string type, string size, string resol, string protect, string oS, string chipset, string cpu, string gPU, string cS, string inter)
        {
            this.Tech = tech;
            this.Announ = announ;
            this.Stat = stat;
            this.Dimensions = dimensions;
            this.Weight = weight;
            this.Build = build;
            SIM1 = sIM;
            Type1 = type;
            this.Size = size;
            this.Resol = resol;
            this.Protect = protect;
            OS1 = oS;
            this.Chipset = chipset;
            Cpu1 = cpu;
            GPU1 = gPU;
            CS1 = cS;
            this.Inter = inter;
        }
    }
}
